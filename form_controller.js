
Drupal.formController = Drupal.formController || {};

/**
 * Attach Form Controller widget and onClick handler to all forms.
 */
Drupal.attachFormController = function() {
  $('form:not(.form-controller-processed)').each(function() {
    var form = $(this);
    form.addClass('form-controller-processed');
    // Prepare the UI.
    var div = $('div.form-controller', form)
      .hide()
      .css('marginLeft', parseInt(form.css('width')) - 22 + 'px');
    form.hover(function() {
    	$('div.form-controller', this).fadeIn();
    }, function() {
    	$('div.form-controller', this).fadeOut();
    });
    $('a', div)
      // We need to shift some form values into the anchor tag to access them 
      // in formControllerClickHandler.
      .attr({target: form.attr('id'), action: form.attr('action')})
      // Attach our onclick handler.
      .click(function(e) {
        return Drupal.formControllerClickHandler(e);
      });
  });
}

/**
 * Load and display settings form for a form.
 */
Drupal.formControllerClickHandler = function(e) {
  // Create container elements for neat styling.
  var position = Drupal.mousePosition(e);
  $('body').append('<div id="form-controller-overlay"></div><div id="form-controller-wrapper"><div id="form-controller-dialog"></div></div>');
  $('#form-controller-overlay, #form-controller-dialog').hide();
  $('#form-controller-overlay').css('height', parseInt($('body').css('height')) + 'px');

  // Load the settings for this form id.
  $('#form-controller-dialog').css('marginTop', position.y + 'px').load(e.currentTarget.href, function() {
    // Attach our submit callback.
    $('#form-controller-form').submit(function() {
      return Drupal.formControllerSubmitHandler(this);
    });
  });
  
  // Store HTML and internal form id for replacement upon completion.
  Drupal.formController.html_id = e.currentTarget.target;
  if (Drupal.settings.formControllerPathPrefix.length == 1) {
    Drupal.formController.form_id = e.currentTarget.pathname.substr(e.currentTarget.pathname.lastIndexOf('/') + 1);
  }
  else {
    Drupal.formController.form_id = e.currentTarget.search.substr(e.currentTarget.search.lastIndexOf('/') + 1);
  }
  // Also backup form action, because FAPI assigns menu callback path by default.
  Drupal.formController.action = $(e.currentTarget).attr('action');

  // Display Form controller.
  $('#form-controller-overlay, #form-controller-dialog').fadeIn('slow');
  // Do not follow original link.
  return false;
}

/**
 * Submit form settings and replace original form on success.
 */
Drupal.formControllerSubmitHandler = function(form) {
  // Submit settings for this form.
  $.post(form.action, $(form).serialize(), function(data) {
    if (data != 1) {
      // Validation error; inject the new form.
      $('#form-controller-dialog').html(data);
      // Re-attach our submit callback.
      $('#form-controller-form').submit(function() {
        return Drupal.formControllerSubmitHandler(this);
      });
    }
    else {
      // Form successfully submitted; remove Form controller.
      $('#form-controller-overlay, #form-controller-wrapper').fadeOut('slow').remove();
      // Now replace the original form.
      $.get(Drupal.settings.formControllerPathPrefix +'form_controller/'+ Drupal.formController.form_id, function(data) {
        $('#'+ Drupal.formController.html_id).replaceWith(data.replace(this.url, Drupal.formController.action));
        Drupal.attachFormController();
      });
    }
  });
  // Do not submit this form.
  return false;
}

/**
 * jQuery <1.2 + jQuery Update compatibility.
 * Note: Coding style not altered for better comparability.
 */
if (typeof jQuery.fn.serializeArray == 'undefined') {
jQuery.fn.extend({
	replaceWith: function( value ) {
		return this.after( value ).remove();
	},

	map: function( callback ) {
		return this.pushStack( jQuery.map(this, function(elem, i){
			return callback.call( elem, i, elem );
		}));
	},

	serialize: function() {
		return jQuery.param(this.serializeArray());
	},
	serializeArray: function() {
		return this.map(function(){
			return jQuery.nodeName(this, "form") ?
				jQuery.makeArray(this.elements) : this;
		})
		.filter(function(){
			return this.name && !this.disabled && 
				(this.checked || /select|textarea/i.test(this.nodeName) || 
					/text|hidden|password/i.test(this.type));
		})
		.map(function(i, elem){
			var val = jQuery(this).val();
			return val == null ? null :
				val.constructor == Array ?
					jQuery.map( val, function(val, i){
						return {name: elem.name, value: val};
					}) :
					{name: elem.name, value: val};
		}).get();
	}
});
}

if (Drupal.jsEnabled) {
  $(document).ready(function() {
    Drupal.attachFormController();
  });
}
