<?php

/**
 * @file
 * Example code for hook_form_controller() implementations.
 */

/**
 * Implementation of hook_form_controller().
 *
 * @param string $op
 *   The operation performed.
 * @param mixed $edit
 *   For operation
 *   - 'settings' a form id.
 *   - 'validate' a form_values array containing only settings of this
 *     implementation.  Use the module as prefix for form_set_error(), e.g.
 *     form_set_error('mymodule][mysetting', ...).
 *   - 'submit' a form_values array containing only settings of this
 *     implementation.
 */
function journal_form_controller($op, $edit = array()) {
  switch ($op) {
    case 'settings':
      $journal_ids = variable_get('journal_form_ids', array());
      $form = array();
      $form['journal'] = array(
        '#type' => 'radios',
        '#title' => t('Journal entry'),
        '#options' => array(
          0 => t('Disable'),
          -1 => t('Allow'),
          1 => t('Required'),
        ),
        '#default_value' => isset($journal_ids[$edit]) ? $journal_ids[$edit] : -1,
        '#description' => t('Whether journal entries for this form should be allowed, required, or completely disabled.'),
      );
      return $form;

    case 'validate':
      // Debug code example:
      // dpm($edit);
      // form_set_error('');
      break;

    case 'submit':
      $journal_ids = variable_get('journal_form_ids', array());
      if ($edit['journal'] != -1) {
        $journal_ids = array_merge($journal_ids, array($edit['#form_id'] => (int)$edit['journal']));
      }
      else {
        unset($journal_ids[$edit['#form_id']]);
      }
      variable_set('journal_form_ids', $journal_ids);
      break;
  }
}

